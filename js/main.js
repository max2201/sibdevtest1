const header = document.querySelector('.header');
const headerHeading = document.querySelector('.header__heading');
let scrollPrev = 0;
//----------------------
const radios = document.querySelectorAll('.radio');
const labels = document.querySelectorAll('.nav-menu__item');
const selector = document.querySelector('.nav-menu__item-selector');
let prevLabel;


radios.forEach((radio, index) => {
  radio.addEventListener('click', function() {
    headerHeading.textContent=labels[index].querySelector('.nav-menu__item-name').textContent;
    if (prevLabel) prevLabel.classList.toggle('active');
    labels[index].classList.toggle('active');
    prevLabel = labels[index];
    selector.style.display="block"
    selector.style.marginTop=`${20+index*64}px`
  });
});




function outerHeight(element) {
  let height = element.offsetHeight;
  const style = getComputedStyle(element);
  height += parseInt(style.marginTop) + parseInt(style.marginBottom);
  return height;
}

window.onscroll = function()
{
  if (!window.matchMedia("(min-width: 991px)").matches){
  if ( pageYOffset > 100 && pageYOffset > scrollPrev) {
    header.classList.add('header_shifted')
    header.style.transform=`translateY(-${outerHeight(headerHeading)}px)`
  } else {
    header.classList.remove('header_shifted')
    header.style.removeProperty('transform')
  }
  scrollPrev = pageYOffset;
}
}
